import Link from "next/link";

export default function Home() {
  return (
    <main className={"bg-white h-screen px-24"}>
      {/** Navbar **/}
      <div className={"header py-12 bg-white text-black w-full flex justify-between"}>
        <div className={"text-2xl"}>halsa</div>
        <div className={"items-center gap-x-8 bg-[#ffd70065] rounded-full border-red-300 px-6 py-2 hidden md:flex"}>
          <Link href={"/"}>Features</Link>
          <Link href={"/"}>About</Link>
          <Link href={"/"}>Members</Link>
          <Link href={"/"}>Blog</Link>
        </div>
        <div className={"bg-[#fbfaf9] rounded-full text-lg px-4 py-2 md:hidden"}>
          Menu
        </div>
        <div className={"bg-[#fbfaf9] rounded-full text-lg px-4 py-2 hidden md:block"}>
          Projects
        </div>
      </div>
      {/** Hero Section **/}
      <section className={"relative overflow-hidden bg-[#fbfaf9] rounded-2xl border border-gray-200 mt-4 pt-16"}>
        <img
          className={"absolute left-0 right-0 top-0 bottom-0 object-cover"}
           src="https://plus.unsplash.com/premium_photo-1687975124216-24f5ead14c41?q=80&w=2940&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
        // src={"https://images.unsplash.com/photo-1682685795557-976f03aca7b2?q=80&w=2942&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"}
         // src="https://assets-global.website-files.com/6544a331d4f63b5b4c8cfa4f/654ceda5362fc31b3855a77d_home-hero.webp"
          alt={"BG-Home"}
        />
        <div className={"z-2 relative"}>
          <div className={"flex justify-center items-center text-6xl text-green-600 font-semibold text-center"}>
            Your Meal Planner
            <br/>
            All In One App
          </div>
          <div className={"flex justify-center items-center text-2xl text-gray-400 text-center my-8 leading-relaxed"}>
            Your Journey to Optimal Living Starts Here:
            <br/>
            Explore Our Integrated Health Hub
          </div>
          <img
            className={"flex justify-center items-center overflow-clip px-32 -mb-12"}
            src={"https://assets-global.website-files.com/6544a331d4f63b5b4c8cfa4f/654cab33855176003ee5486b_app.webp"}
            alt={"Snapshot"}
            sizes="(max-width: 479px) 79vw, (max-width: 767px) 83vw, (max-width: 1439px) 82vw, 80vw"
          />
        </div>
      </section>
      <section>

      </section>
    </main>
  );
}
